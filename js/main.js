$(document).ready(function(){
	$("div.box-text").mouseover(function(){
		$(this).css("background-color","#ffffff");
		$(this).find("i").css("color","#e8645a");
		$(this).find(".rm-box span").css({"background-color":"#e8645a",
			"-webkit-box-shadow": "0px 3px 3px 0px rgba(226,83,75,1)",
			"-moz-box-shadow": "0px 3px 3px 0px rgba(226,83,75,1)",
			"box-shadow": "0px 3px 3px 0px rgba(226,83,75,1)"});
	});
	$("div.box-text").mouseout(function(){
		$(this).css("background-color","#f8f8f8");
		$(this).find("i").css("color","#62bdbd");
		$(this).find(".rm-box span").css({"background-color":"#62bdbd",
			"-webkit-box-shadow": "0px 3px 3px 0px rgba(98,185,176,1)",
			"-moz-box-shadow": "0px 3px 3px 0px rgba(98,185,176,1)",
			"box-shadow": "0px 3px 3px 0px rgba(98,185,176,1)"});
	});
	$("#slider").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      navigationText: ['<i class="fa fa-angle-double-left"></i>', '<i class="fa fa-angle-double-right"></i>'],
      autoPlay:true,
      touchDrag: true
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
  });
	$('.mob_menu').on('click', function(){
		$('.menu-main ul.manu-second').slideToggle('slow');
	});
	$('.menu-main ul.manu-second li').on('click', function(){ 
		$(this).has('ul').find('ul').slideToggle('slow');
	});

});